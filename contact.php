<?php include 'header/header_2.php' ;?>
    <section class="flat-benefit flat-benefit-style5 pt0 clearfix" style="margin-top: 20px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-benefit-right" style="width: 100%">
                        <div class="apply-admission bg-apply-type1">
                            <div class="apply-admission-wrap type5 bd-type2">
                                <div class="apply-admission-inner">
                                    <h2 class="title text-center">
                                        <span>Contact Us</span>
                                    </h2>
                                </div>
                            </div>
                            <div class="form-apply">
                                <div class="section-overlay333"></div>
                                <form action="contact_save_data.php" class="apply-now" method="POST">
                                    <ul>
                                        <li><input type="search" name="name" placeholder="Name" required></li>
                                        <li><input type="search" name="email" placeholder="Email" required></li>
                                        <li><input type="search" name="phone" placeholder="Phone" required></li>
                                        <li><input type="search" name="message" placeholder="message" required></li>
    
                                    </ul>
                                    <div class="btn-50 hv-border text-center">
                                        <button class="btn bg-clf0c41b">
                                            Send
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3770.49757387517!2d72.88696021418521!3d19.085815487082332!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3be7c78ae4eca899%3A0x300fdca9125ccb45!2sTechStalwarts%20Software%20Development%20LLP!5e0!3m2!1sen!2sin!4v1566995899708!5m2!1sen!2sin" width="600" height="650" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                </div>
            </div>
    
    
        </div>
    </section>
    
    <div class="cta-cr parallax parallax3">
        <div class="overlay183251"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
                    <div class="cta-content">
                        <div class="caption">How to start your currier?</div>
                        <h3>
                            Starting your journey with us? follow this guide still possible to achieve your goals.
                        </h3>
                        <div class="btn-about-become">
                            <a href="join.php">Join Now</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
                    <div class="cta-information">
                        <div class="phone">
                            +91 99306 02530
                        </div>
                        <div class="email">
                            Business@techstalwarts.com
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- cta-cr -->
<?php include 'footer/footer.php'; ?>
</body>

<!-- Mirrored from corpthemes.com/html/edukin/course.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Mar 2020 07:10:56 GMT -->
</html>