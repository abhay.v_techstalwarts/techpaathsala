<?php 
session_start();

if(!isset($_SESSION['email'])){
   header("Location:login.php");
}
else{
    unset($_SESSION["email"]);
   unset($_SESSION["password"]);
}

require_once("config.php");  
$sql = "SELECT * FROM contact"; 
$sql2 = "SELECT * FROM recuruit"; 
// $res =mysqli_query($conn,$sql);
// $result = mysqli_fetch_array($res);
// $result = $conn->query($sql);

// $result1 = mysqli_fetch_array($result);
// print_r($result1);
// $vs = array($result);
// if ($vs) {
//     print_r($result);
// } else {
//     echo "Error: " . $vs . "<br>" . mysqli_error($conn);
// }
$result = $conn->query($sql);
$result2 = $conn->query($sql2);
mysqli_close($conn);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Techpaathsala show</title>

    <!-- Mobile Specific Metas-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <!-- Bootstrap-->
    <link rel="stylesheet" href="stylesheet/bootstrap.css">

    <!-- Template Style-->
    <link rel="stylesheet" href="stylesheet/font-awesome.css"> 
    <link rel="stylesheet" href="stylesheet/animate.css">
    <link rel="stylesheet" href="stylesheet/style.css">
    <link rel="stylesheet" href="stylesheet/shortcodes.css">
    <link rel="stylesheet" href="stylesheet/jquery-fancybox.css">
    <link rel="stylesheet" href="stylesheet/responsive.css">
    <link rel="stylesheet" href="stylesheet/flexslider.css">
    <link rel="stylesheet" href="stylesheet/owl.theme.default.min.css">
    <link rel="stylesheet" href="stylesheet/owl.carousel.min.css">
    <link rel="stylesheet" href="stylesheet/jquery.mCustomScrollbar.min.css">

    <link type="image/png" href="icon/favicon.png" rel="shortcut icon">
</head>
<body>
    <div id="loading-overlay">
        <div class="loader"></div>
    </div>
    <section class="flat-benefit flat-benefit-style5 clearfix">
        <div class="container">
            <div class="row">
                <div class="title" style="padding:20px 20px; font-size:20px;color:black;font-weight:700;">
                    <h2>Contact form Data</h2>
                </div>
                <div class="col-md-12">
                <table class="table table-hover">
                        <thead>
                          <tr>
                            <th>name</th>
                            <th>email</th>
                            <th>phone</th>
                            <th>message</th>
                          </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if ($result->num_rows > 0) {
                                // output data of each row
                                while($row = $result->fetch_assoc()) {
                                    // echo "Name: " . $row["name"]. " email" . $row["email"]."phone". $row["phone"]."message". $row["message"]. "<br>";
                              echo '<tr>
                              <td>'.$row["id"].'</td>
                              <td>'.$row["name"].'</td>
                              <td>'.$row["email"].'</td>
                              <td>'.$row["phone"].'</td>
                              <td>'. $row["message"].'</td>
                              </tr>
                              ';
                                
                                }
                            } else {
                                echo "0 results";
                            }
                            ?>
                         
                        </tbody>
                      </table>
                    </div>
                    
                </div>
        </div>
    </section>
    <section class="flat-benefit flat-benefit-style5 clearfix">
        <div class="container">
            <div class="row">
            <div class="title" style="padding:20px 20px; font-size:20px;color:black;font-weight:700;">
                    <h2>recuruit Form Data</h2>
                </div>
                <div class="col-md-12">
                <table class="table table-hover">
                        <thead>
                          <tr>
                          <th>Id</th>
                            <th>componey name</th>
                            <th>email</th>
                            <th>phone</th>
                            <th>orgnation name</th>
                            <th>orgnation website</th>
                            <th>Looking For Hire Students</th>
                            <th>No of student</th>
                            <th>Organisation Type</th>

                          </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if ($result2->num_rows > 0) {
                                // output data of each row
                                while($row = $result2->fetch_assoc()) {
                                    // echo "Name: " . $row["name"]. " email" . $row["email"]."phone". $row["phone"]."message". $row["message"]. "<br>";
                              echo '<tr>
                              <td>'.$row["id"].'</td>
                              <td>'.$row["componeyname"].'</td>
                              <td>'.$row["email"].'</td>
                              <td>'.$row["phone"].'</td>
                              <td>'. $row["orgname"].'</td>
                              <td>'. $row["orgwebsite"].'</td>
                              <td>'. $row["orgwebsite"].'</td>
                              <td>'. $row["hirestudentfrom"].'</td>
                              <td>'. $row["noofstudent"].'</td>
                              <td>'. $row["orgtype"].'</td>


                              </tr>
                              ';
                                
                                }
                            } else {
                                echo "0 results";
                            }
                            ?>
                         
                        </tbody>
                      </table>
                    </div>
                    
                </div>
        </div>
    </section>
    <script src="javascript/jquery.min.js"></script>
    <script src="javascript/rev-slider.js"></script>
    <script src="javascript/plugins.js"></script>
    <script src="javascript/jquery-countTo.js"></script>
    <script src="javascript/jquery-ui.js"></script>
    <script src="javascript/jquery-fancybox.js"></script>
    <script src="javascript/flex-slider.min.js"></script>
    <script src="javascript/scroll-img.js"></script>
    <script src="javascript/owl.carousel.min.js"></script>
    <script src="javascript/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="javascript/parallax.js"></script>
    <script src="javascript/jquery-isotope.js"></script>
    <script src="javascript/equalize.min.js"></script>
    <script src="javascript/main.js"></script>


    <!-- slider -->
    <script src="rev-slider/js/jquery.themepunch.tools.min.js"></script>
    <script src="rev-slider/js/jquery.themepunch.revolution.min.js"></script>

    <!-- Load Extensions only on Local File Systems ! The following part can be removed on Server for On Demand Loading -->
    <script src="rev-slider/js/extensions/extensionsrevolution.extension.actions.min.js"></script>
    <script src="rev-slider/js/extensions/extensionsrevolution.extension.carousel.min.js"></script>
    <script src="rev-slider/js/extensions/extensionsrevolution.extension.kenburn.min.js"></script>
    <script src="rev-slider/js/extensions/extensionsrevolution.extension.layeranimation.min.js"></script>
    <script src="rev-slider/js/extensions/extensionsrevolution.extension.migration.min.js"></script>
    <script src="rev-slider/js/extensions/extensionsrevolution.extension.navigation.min.js"></script>
    <script src="rev-slider/js/extensions/extensionsrevolution.extension.parallax.min.js"></script>
    <script src="rev-slider/js/extensions/extensionsrevolution.extension.slideanims.min.js"></script>
    <script src="rev-slider/js/extensions/extensionsrevolution.extension.video.min.js"></script>
</body>
</html>