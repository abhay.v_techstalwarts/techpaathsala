<?php include 'header/header_2.php' ;?>
    <div class="courses-single-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="content-page-wrap clearfix">
                        <div class="course-single">
                            <div class="featured-post">
                                <div class="entry-image">
                                    <div class="videobox">
                                        <a class="fancybox" data-type="iframe" href="https://www.youtube.com/embed/2Ge1GGitzLw?autoplay=1">
                                            <img src="images/course-single/1.jpg" alt="images">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="#">Learn Angular</a>
                                </div>
                            </div>
                        </div>
                        <div class="flat-tabs">
                            <ul class="tab-title type1 clearfix"> 
                                <li class="item-title  overview">
                                    <span class="inner">OVERVIEW</span>
                                </li>
                                <li class="item-title curriculum">
                                    <span class="inner">CURRICULUM</span>
                                </li>
                            </ul>
                            <div class="tab-content-wrap">
                                <div class="tab-content">
                                    <div class="item-content">
                                        <div class="question-sg text clearfix">
                                            <div class="title">
                                                <a href="#">Program Overview</a>
                                            </div>
                                            <p>
                                                The Advanced AngularJS course teaches developers to leverage AngularJS to its fullest potential to build testable, scalable, and full-stack web applications.  The course begins with a quick review of key architectural concepts, characteristics, and elements of the AngularJS framework. It then transitions into an in-depth examination of the more complex elements of the framework, such as directives, User Interface (UI)-router and services. 
                                                To register for course one should know Angular and want to take their expertise to the next level.

                                            </p>
                                        </div>
                                   
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div class="item-content">
                                        <div class="question-sg text clearfix">
                                            <div class="title">
                                                <a href="#">
                                                    Introduction</a>
                                            </div>
                                            <p>
                                                Review of core AngularJS key concepts
                                            </p>
                                        </div>
                           
                                        <div class="certificate-sg text clearfix">
                                            <div class="title">
                                                <a href="#">You'll learn:</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            MVW framework
                                                        </li>
                                                        <li>
                                                            Directives
                                                        </li>
                                                        <li>
                                                            Controllers
                                                        </li>
                                                        <li>
                                                            Services
                                                        </li>
                                                   
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Advanced Directives</a>
                                            </div>
                                            <!-- <p>
                                                In this module, you will learn to build and style medium clap
                                            </p><br> -->
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Binding strategies
                                                        </li>
                                                        <li>
                                                            Wrapping 3rd party libraries
                                                        </li>
                                                        <li>
                                                            Transclusion
                                                        </li>
                                                        <li>
                                                            Controllers
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">UI-Router</a>
                                            </div>
                                            <!-- <p>
                                                Building your own Hooks lets you extract component logic into reusable functions. This module provides an overview of Hooks for experienced React users.
                                            </p><br> -->
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            UI-Router nested views
                                                        </li>
                                                        <li>
                                                            UI-Router multiple views
                                                        </li>
                                                        <li>
                                                            UI-Router extras
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Forms</a>
                                            </div>
                                            <!-- <p>
                                                This module aims to help you understand the compound components pattern so that you can use it effectively with confidence and clarity.
                                            </p><br> -->
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Custom form validation
                                                        </li>
                                                        <li>
                                                            Custom form control
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Services</a>
                                            </div>
                                            <!-- <p>
                                                This module is all about how to implement the styling of React components.
                                            </p><br> -->
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Provider Service
                                                        </li>
                                                        <li>
                                                            Service Decorator
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Web Services</a>
                                            </div>
                                            <!-- <p>
                                                In this module, you will learn what a container component is and also how it works.
                                            </p><br> -->
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Restangular
                                                        </li>
                                                        <li>
                                                            Messaging
                                                        </li>
                                                        <li>
                                                            Promises
                                                        </li>
                                                      
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Best Practices with Angular</a>
                                            </div>
                                            <!-- <p>
                                                The data inside React Components are managed by state and props. After understanding props, in this module, you will learn about the state.
                                            </p><br> -->
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Angular JS design Patterns
                                                        </li>
                                                        <li>
                                                            Effective angular testing strategies
                                                        </li>
                                                        <li>
                                                            Controller-as-syntax


                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div><!-- courses-single -->
    <div class="cta-cr parallax parallax3">
        <div class="overlay183251"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
                    <div class="cta-content">
                        <div class="caption">How to start your teaching?</div>
                        <h3>
                            Starting your journey with us? Follow this guide still possible to become a teacher.
                        </h3>
                        <div class="btn-about-become">
                            <a href="#">Become a Teacher</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
                    <div class="cta-information">
                        <div class="phone">
                            +91 254 785 587
                        </div>
                        <div class="email">
                            edukin@info.com
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- cta-cr -->
    <?php include 'footer/footer.php'; ?>
</body>

<!-- Mirrored from corpthemes.com/html/edukin/course-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Mar 2020 07:11:11 GMT -->
</html>