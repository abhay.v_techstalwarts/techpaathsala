<?php include 'header/header_2.php' ?>
    <div class="courses-single-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="content-page-wrap clearfix">
                        <div class="course-single">
                            <div class="featured-post">
                                <div class="entry-image">
                                    <div class="videobox">
                                        <a class="fancybox" data-type="iframe" href="https://www.youtube.com/embed/2Ge1GGitzLw?autoplay=1">
                                            <img src="images/course-single/1.jpg" alt="images">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="#">Learn Angular</a>
                                </div>
                            </div>
                        </div>
                        <div class="flat-tabs">
                            <ul class="tab-title type1 clearfix"> 
                                <li class="item-title  overview">
                                    <span class="inner">OVERVIEW</span>
                                </li>
                                <li class="item-title curriculum">
                                    <span class="inner">CURRICULUM</span>
                                </li>
                            </ul>
                            <div class="tab-content-wrap">
                                <div class="tab-content">
                                    <div class="item-content">
                                        <div class="question-sg text clearfix">
                                            <div class="title">
                                                <a href="#">Program Overview</a>
                                            </div>
                                            <p>
                                                This course is designed to start you on a path toward future studies in web development and design.
                                                This course is specially designed fo Beginners. By the end of this course you’ll be able to describe the structure 
                                                and functionality of the world wide web, create dynamic web pages using a combination of HTML, CSS, and JavaScript, 
                                                apply essential programming language concepts when creating HTML forms, select an appropriate web hosting service, 
                                                and publish your webpages for the world to see. Finally, you’ll be able to develop a working model for creating your own 
                                                personal or business websites in the future and be fully prepared to take the next step in a more advanced web development 
                                                or design course or specialization.
                                            </p>
                                        </div>
                                   
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div class="item-content">
                                        <div class="question-sg text clearfix">
                                            <div class="title">
                                                <a href="#">
                                                    Course Overview and Website Structure and Hosting:</a>
                                            </div>
                                            <p>
                                                This first module provides an overview of how websites function, their structure, and the ins and outs of choosing a 
                                                website name and selecting an online host to house your website.
                                            </p>
                                        </div>
                           
                                        <div class="certificate-sg text clearfix">
                                            <div class="title">
                                                <a href="#">You'll learn:</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Domain Buying
                                                        </li>
                                                        <li>
                                                            Web Hosting
                                                        </li>
                                                        <li>
                                                            Nettwork  and Internet Function at a high level 
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Designing Your Own Website: HTML Basics</a>
                                            </div>
                                            <p>
                                                In this module, we'll begin to explore how to design and create websites by exploring the base language used to power all 
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            HTML tags
                                                        </li>
                                                        <li>
                                                            Adding Images to web pages
                                                        </li>
                                                        <li>
                                                            Hyperlink
                                                        </li>
                                                        <li>
                                                            Uploading webpages to a web host 
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Introduction to Programming Using JavaScript</a>
                                            </div>
                                            <p>
                                                Now that you know some basic HTML, it's time to turn our attention to another common scripting language used to make 
                                                websites dynamic - that is allowing users to interact with your webpages - JavaScript. While learning about JavaScript, you'll also 
                                                gain some foundational knowledge common to all programming languages.
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Create simple JavaScript programs
                                                        </li>
                                                        <li>
                                                            JavaScript to set up alerts and respond to events
                                                        </li>
                                                        <li>
                                                            To read input, and to change HTML
                                                        </li>
                                                        <li>
                                                            Basic JavaScript Testing
                                                        </li>
                                                       
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Websites with Style: CSS Properties, Colors and Fonts</a>
                                            </div>
                                            <p>
                                                While HTML and JavaScript are very useful for web development, they don't exactly make websites look attractive - that's 
                                                where cascading style sheets, or CSS, comes into play. While HTML is used to build the structure of our pages and JavaScript is
                                                 used to provide interactive functionality, CSS is used to graphically design and layout webpages. 
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Identify and apply CSS basics like purpose and syntax; 
                                                        </li>
                                                        <li>
                                                            CSS properties to control fonts, colors, layouts, and other common properties
                                                        </li>
                                                        <li>
                                                            Differentiate between in-line, internal, and external CSS
                                                        </li>
                                                        <li>
                                                            Practice and test your cascading style sheets
                                                        </li>  
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div><!-- courses-single -->
    <div class="cta-cr parallax parallax3">
        <div class="overlay183251"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
                    <div class="cta-content">
                        <div class="caption">How to start your teaching?</div>
                        <h3>
                            Starting your journey with us? Follow this guide still possible to become a teacher.
                        </h3>
                        <div class="btn-about-become">
                            <a href="#">Become a Teacher</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
                    <div class="cta-information">
                        <div class="phone">
                            +91 254 785 587
                        </div>
                        <div class="email">
                            edukin@info.com
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- cta-cr -->
    <?php include 'footer/footer.php' ?>
</body>

<!-- Mirrored from corpthemes.com/html/edukin/course-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Mar 2020 07:11:11 GMT -->
</html>