    <?php include 'header/header_2.php' ?>
    <div class="course-grid">
        <div class="container">
            <div class="flat-portfolio">
                <ul class="flat-filter-isotype">
                    <li class="active"><a href="#" data-filter="*">Show all</a></li>
                    <li><a href="#" data-filter=".Certificate">Certificate</a></li>
                    <li><a href="#" data-filter=".Marketing"> Marketing </a></li>
                    <li><a href="#" data-filter=".Popular"> Popular </a></li>
                    <li><a href="#" data-filter=".Learning">Learning</a></li>
                </ul>
                <div class="search-course">
                    <form action="#" class="search-form">
                        <input type="search" placeholder="Search here....">
                        <button class="search-button">
                            <i class="fa fa-search" aria-hidden="true"></i> 
                        </button>
                    </form>
                </div>
            </div>
            <div class="flat-courses clearfix isotope-courses">
                <div class="course clearfix Marketing Certificate Popular">    
                    <div class="flat-course">
                        <div class="featured-post post-media">
                            <div class="entry-image pic">
                                <img src="images/course-grid/1.jpg" alt="images">
                                <div class="hover-effect"></div>
                            </div>
                        </div>
                        <div class="course-content clearfix">
                            <div class="wrap-course-content">
                                <h4>
                                    <a href="fullstack.php">Full Stack Development </a>
                                </h4>
                                <!-- <p>
                                    Education City is initiative of our Qatar Foundation for Education, Science and Community Development. 
                                </p> -->
                                <div class="author-info">
                                    <div class="enroll">
                                        <a href="contact.php">Enroll</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="course clearfix Marketing">    
                    <div class="flat-course">
                        <div class="featured-post post-media active">
                            <div class="entry-image pic">
                                <img src="images/course-grid/2.jpg" alt="images">
                                <div class="hover-effect"></div>
                            </div>
                        </div>
                        <div class="course-content clearfix">
                            <div class="wrap-course-content">
                                <h4>
                                    <a href="advance_nodejs.php">Node Js</a>
                                </h4>
                                <!-- <p>
                                    Education City is initiative of our Qatar Foundation for Education, Science and Community Development. 
                                </p> -->
                                <div class="author-info">
                                    <div class="enroll">
                                        <a href="contact.php">Enroll</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="course clearfix Popular Certificate">    
                    <div class="flat-course">
                        <div class="featured-post post-media">
                            <div class="entry-image pic">
                                <img src="images/course-grid/3.jpg" alt="images">
                                <div class="hover-effect"></div>
                            </div>
                        </div>
                        <div class="course-content clearfix">
                            <div class="wrap-course-content">
                                <h4>
                                    <a href="php.php">PHP</a>
                                </h4>
                                <!-- <p>
                                    Education City is initiative of our Qatar Foundation for Education, Science and Community Development. 
                                </p> -->
                                <div class="author-info">
                                    <div class="enroll">
                                        <a href="contact.php">Enroll</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="course clearfix Marketing Certificate Learning">    
                    <div class="flat-course">
                        <div class="featured-post post-media">
                            <div class="entry-image pic">
                                <img src="images/course-grid/4.jpg" alt="images">
                                <div class="hover-effect"></div>
                            </div>
                        </div>
                        <div class="course-content clearfix">
                            <div class="wrap-course-content">
                                <h4>
                                    <a href="reactnative.php">React Native</a>
                                </h4>
                                <!-- <p>
                                    Education City is initiative of our Qatar Foundation for Education, Science and Community Development.  
                                </p> -->
                                <div class="author-info">
                                    <div class="enroll">
                                        <a href="contact.php">Enroll</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="course clearfix Marketing Certificate">    
                    <div class="flat-course">
                        <div class="featured-post post-media">
                            <div class="entry-image pic">
                                <img src="images/course-grid/5.jpg" alt="images">
                                <div class="hover-effect"></div>
                            </div>
                        </div>
                        <div class="course-content clearfix">
                            <div class="wrap-course-content">
                                <h4>
                                    <a href="angularjs.php">Angular JS</a>
                                </h4>
                                <!-- <p>
                                    Education City is initiative of our Qatar Foundation for Education, Science and Community Development. 
                                </p> -->
                                <div class="author-info">
                                    <div class="enroll">
                                        <a href="contact.php">Enroll</a>
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div> 
                <div class="course clearfix Marketing Certificate">    
                    <div class="flat-course">
                        <div class="featured-post post-media">
                            <div class="entry-image pic">
                                <img src="images/course-grid/6.jpg" alt="images">
                                <div class="hover-effect"></div>
                            </div>
                        </div>
                        <div class="course-content clearfix">
                            <div class="wrap-course-content">
                                <h4>
                                    <a href="webdesigner.php">Web Designer</a>
                                </h4>
                                <div class="author-info">
                                    <div class="enroll">
                                        <a href="contact.php">Enroll</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </div> 
            <!-- <div class="pagination">
                <ul>
                    <li><a href="#" class="page-numbers current">1</a></li>
                    <li><a href="#" class="page-numbers">2</a></li>
                    <li><a href="#" class="page-numbers">3</a></li>
                    <li><a href="#" class="page-numbers">4</a></li>
                    <li><a href="#" class="page-numbers">5</a></li>
                    <li><a href="#" class="page-numbers">6</a></li>
                </ul>
            </div> -->
        </div>
    </div><!-- course-grid -->

    <div class="cta-cr parallax parallax3">
        <div class="overlay183251"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
                    <div class="cta-content">
                        <div class="caption">How to start your teaching?</div>
                        <h3>
                            Starting your journey with us? Follow this guide still possible to become a teacher.
                        </h3>
                        <div class="btn-about-become">
                            <a href="#">Become a Teacher</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
                    <div class="cta-information">
                        <div class="phone">
                            +91 254 785 587
                        </div>
                        <div class="email">
                            edukin@info.com
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- cta-cr -->

  <?php include 'footer/footer.php'; ?>
</body>

<!-- Mirrored from corpthemes.com/html/edukin/course.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Mar 2020 07:10:56 GMT -->
</html>