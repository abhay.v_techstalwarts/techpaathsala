<?php include 'header/header_2.php' ?>
    <div class="courses-single-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="content-page-wrap clearfix">
                        <div class="course-single">
                            <div class="featured-post">
                                <div class="entry-image">
                                    <div class="videobox">
                                        <a class="fancybox" data-type="iframe" href="https://www.youtube.com/embed/2Ge1GGitzLw?autoplay=1">
                                            <img src="images/course-single/1.jpg" alt="images">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="#">Learn Angular</a>
                                </div>
                            </div>
                        </div>
                        <div class="flat-tabs">
                            <ul class="tab-title type1 clearfix"> 
                                <li class="item-title  overview">
                                    <span class="inner">OVERVIEW</span>
                                </li>
                                <li class="item-title curriculum">
                                    <span class="inner">CURRICULUM</span>
                                </li>
                            </ul>
                            <div class="tab-content-wrap">
                                <div class="tab-content">
                                    <div class="item-content">
                                        <div class="question-sg text clearfix">
                                            <div class="title">
                                                <a href="#">Program Overview</a>
                                            </div>
                                            <p>
                                                The PHP Hypertext Preprocessor (PHP) is a programming language that allows web developers to create dynamic content that interacts with databases.
                                                This course is specially designed for Beginners.
                                                In this course, you'll explore the basic structure of a web application, and how a web browser interacts with a web server. 
                                                You'll be introduced to the request/response cycle, including GET/POST/Redirect. You'll also gain an introductory understanding of basic 
                                                syntax and data structures of the PHP language, variables, logic, iteration, arrays, error handling, and superglobal variables, among other elements. 
                                            </p>
                                        </div>
                                   
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div class="item-content">
                                        <div class="question-sg text clearfix">
                                            <div class="title">
                                                <a href="#">Php Overview & Fundamentals </a>
                                            </div>
                                            <p>
                                                This first module provides an overview of php, local environmen tools, code editor installations,using dynamic data in php.
                                            </p>
                                        </div>
                           
                                        <div class="certificate-sg text clearfix">
                                            <div class="title">
                                                <a href="#">You'll learn:</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Php Fundamentals
                                                        </li>
                                                        <li>
                                                            Code Editor (XAMP/WAMP)
                                                        </li>
                                                        <li>
                                                            Embedding PHP in HTML
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Php Data Types & More</a>
                                            </div>
                                            <p>
                                                In this module you'll learn about  variables and writing simple loops and functions, before building up to coding 
                                                classes for simple object-oriented programming (OOP).
                                                We look at unique aspects of arrays,functions & variables in the PHP language. 
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Arrays
                                                        </li>
                                                        <li>
                                                            Functions
                                                        </li>
                                                        <li>
                                                            Variable
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Php Control Structures:</a>
                                            </div>
                                            <p>
                                                Control structures are core features of the PHP language that allows your script to respond differently to different inputs or situations. This module will give you overall idea of all control functions used in PHP.
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            If Statement
                                                        </li>
                                                        <li>
                                                            Comparison & Logical Operators
                                                        </li>
                                                        <li>
                                                            Switch Statements
                                                        </li>
                                                        <li>
                                                            While,For,Foreach loop
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Php & MySql </a>
                                            </div>
                                            <p>
                                                You'll learn all the most important skills for writing apps for the web: you'll get a chance to practice responding to 
                                                GET and POST requests, parsing JSON, authenticating users, and using a MySQL database. 
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Creating Database
                                                        </li>
                                                        <li>
                                                            Inserting Database
                                                        </li>
                                                        <li>
                                                            Binding
                                                        </li>
                                                        <li>
                                                            Updating Records
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div><!-- courses-single -->
    <div class="cta-cr parallax parallax3">
        <div class="overlay183251"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
                    <div class="cta-content">
                        <div class="caption">How to start your teaching?</div>
                        <h3>
                            Starting your journey with us? Follow this guide still possible to become a teacher.
                        </h3>
                        <div class="btn-about-become">
                            <a href="#">Become a Teacher</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
                    <div class="cta-information">
                        <div class="phone">
                            +91 254 785 587
                        </div>
                        <div class="email">
                            edukin@info.com
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- cta-cr -->
    <?php include 'footer/footer.php' ?>
</body>

<!-- Mirrored from corpthemes.com/html/edukin/course-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Mar 2020 07:11:11 GMT -->
</html>