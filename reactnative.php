<?php include 'header/header_2.php' ;?>
    <div class="courses-single-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="content-page-wrap clearfix">
                        <div class="course-single">
                            <div class="featured-post">
                                <div class="entry-image">
                                    <div class="videobox">
                                        <a class="fancybox" data-type="iframe" href="https://www.youtube.com/embed/2Ge1GGitzLw?autoplay=1">
                                            <img src="images/course-single/1.jpg" alt="images">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="#">Learn Angular</a>
                                </div>
                            </div>
                        </div>
                        <div class="flat-tabs">
                            <ul class="tab-title type1 clearfix"> 
                                <li class="item-title  overview">
                                    <span class="inner">OVERVIEW</span>
                                </li>
                                <li class="item-title curriculum">
                                    <span class="inner">CURRICULUM</span>
                                </li>
                            </ul>
                            <div class="tab-content-wrap">
                                <div class="tab-content">
                                    <div class="item-content">
                                        <div class="question-sg text clearfix">
                                            <div class="title">
                                                <a href="#">Program Overview</a>
                                            </div>
                                            <p>
                                                React Native is a JavaScript framework for building native mobile apps. It uses the React framework and offers a large number of inbuilt components and APIs.
                                                This tutorial is designed for JavaScript and React developers who aspire to learn mobile building skills.
                                                To join this course one should know about react components.

                                            </p>
                                        </div>
                                   
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div class="item-content">
                                        <div class="question-sg text clearfix">
                                            <div class="title">
                                                <a href="#">
                                                    Introduction</a>
                                            </div>
                                            <p>
                                                This will be an introductory session of advanced react patterns
                                            </p>
                                        </div>
                           
                                        <div class="certificate-sg text clearfix">
                                            <div class="title">
                                                <a href="#">You'll learn:</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Advanced React Patterns
                                                        </li>
                                                        <li>
                                                            Design Patterns & React
                                                        </li>
                                                        <li>
                                                            The medium clap
                                                        </li>
                                                   
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Building The Medium Clap</a>
                                            </div>
                                            <p>
                                                In this module, you will learn to build and style medium clap
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Setting up your local environment 
                                                        </li>
                                                        <li>
                                                            How the demo showcase works
                                                        </li>
                                                        <li>
                                                            Handling user interactivity
                                                        </li>
                                                        <li>
                                                            Creating & updating the animation timeline
                                                        </li>
                                                        <li>
                                                            Animating the clap count
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Custom Hooks</a>
                                            </div>
                                            <p>
                                                Building your own Hooks lets you extract component logic into reusable functions. This module provides an overview of Hooks for experienced React users.
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Introduction to custom hooks
                                                        </li>
                                                        <li>
                                                            Building an animation custom hook
                                                        </li>
                                                        <li>
                                                            Why reusability fails
                                                        </li>
                                                        <li>
                                                            Custom hooks & refs
                                                        </li>
                                                        <li>
                                                            useDOMRef

                                                        </li>
                                                        <li>
                                                            useClapState
                                                        </li>
                                                        <li>
                                                            useEffectAftterMount
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Compound Components</a>
                                            </div>
                                            <p>
                                                This module aims to help you understand the compound components pattern so that you can use it effectively with confidence and clarity.
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Introduction to compound components
                                                        </li>
                                                        <li>
                                                            How to implement the pattern
                                                        </li>
                                                        <li>
                                                            Refactor to compound components
                                                        </li>
                                                        <li>
                                                            Alternative export strategy
                                                        </li>
                                                        <li>
                                                            Invoking the useEffect callback only after mount
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Crafting Reusable Styles</a>
                                            </div>
                                            <p>
                                                This module is all about how to implement the styling of React components.
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Introduction to reusable styles
                                                        </li>
                                                        <li>
                                                            Extending styles via a style prop
                                                        </li>
                                                        <li>
                                                            Extending styles via a className prop
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Control Props  and Props Getters</a>
                                            </div>
                                            <p>
                                                In this module, you will learn what a container component is and also how it works.
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            What are control props & props getters?
                                                        </li>
                                                        <li>
                                                            Implementing the pattern
                                                        </li>
                                                        <li>
                                                            Practical usage of props
                                                        </li>
                                                        <li>
                                                            Form collections to getters
                                                        </li>
                                                        <li>
                                                            Use cases for prop getters.
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">State Initializers</a>
                                            </div>
                                            <p>
                                                The data inside React Components are managed by state and props. After understanding props, in this module, you will learn about the state.
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            What are state initializers
                                                        </li>
                                                        <li>
                                                            First pattern requirement
                                                        </li>
                                                        <li>
                                                            Handling resets
                                                        </li>
                                                        <li>
                                                            Handlin reset side effects
                                                        </li>
                                                        <li>
                                                            How usePrevious works
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div><!-- courses-single -->
    <div class="cta-cr parallax parallax3">
        <div class="overlay183251"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
                    <div class="cta-content">
                        <div class="caption">How to start your teaching?</div>
                        <h3>
                            Starting your journey with us? Follow this guide still possible to become a teacher.
                        </h3>
                        <div class="btn-about-become">
                            <a href="#">Become a Teacher</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
                    <div class="cta-information">
                        <div class="phone">
                            +91 254 785 587
                        </div>
                        <div class="email">
                            edukin@info.com
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- cta-cr -->
    <?php include 'footer/footer.php'; ?>
</body>

<!-- Mirrored from corpthemes.com/html/edukin/course-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Mar 2020 07:11:11 GMT -->
</html>