
    <?php include 'header/header_1.php';?>
    <section class="flat-slider style1 clearfix">
        <div class="rev_slider_wrapper fullwidthbanner-container" >
            <div id="rev-slider1" class="rev_slider fullwidthabanner">
                <ul>
                    <!-- Slide 1 -->
                    <li data-transition="random">
                        <!-- Main Image -->
                        <img src="images/header/02.png" alt="" data-bgposition="center center" data-no-retina>
                        <div class="overlay"></div>

                        <!-- Layers -->
                        <div class="tp-caption tp-resizeme education"
                        data-x="['left','left','left','center']" data-hoffset="['0','4','4','15']"
                        data-y="['middle','middle','middle','middle']" data-voffset="['-113','-80','-70','-70']"
                        data-fontsize="['70','70','50','30']"
                        data-lineheight="['90','90','70','50']"
                        data-width="full"
                        data-height="none"
                        data-whitespace="normal"
                        data-transform_idle="o:1;"
                        data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                        data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                        data-mask_in="x:0px;y:[100%];" 
                        data-mask_out="x:inherit;y:inherit;" 
                        data-start="1000" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on"> <div class="education-text text-white">Education for all</div> </div>

                        <div class="tp-caption tp-resizeme text-white complete text-edukin"
                        data-x="['left','left','left','center']" data-hoffset="['0','4','4','15']"
                        data-y="['middle','middle','middle','middle']" data-voffset="['-22','-5','-10','-10']"
                        data-fontsize="['17','17','15','14']"
                        data-lineheight="['30','30','26','22']"
                        data-width="full"
                        data-height="none"
                        data-whitespace="normal"
                        data-transform_idle="o:1;"
                        data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                        data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                        data-mask_in="x:0px;y:[100%];" 
                        data-mask_out="x:inherit;y:inherit;" 
                        data-start="1000" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on" 
                        data-paddingright="['550','155','50','2']" >Complete your educution record collection. Discover Educution's full discography. Education is not key to success in life.</div>

                        <div class="tp-caption"
                        data-x="['left','left','left','center']" data-hoffset="['0','4','4','15']"
                        data-y="['middle','middle','middle','middle']" data-voffset="['88','80','60','70']"
                        data-width="full"
                        data-height="none"
                        data-whitespace="normal"
                        data-transform_idle="o:1;"
                        data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                        data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                        data-mask_in="x:0px;y:[100%];" 
                        data-mask_out="x:inherit;y:inherit;" 
                        data-start="1000" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on" 
                        data-paddingtop= "['50','50','50','50']"
                        data-paddingbottom= "['50','50','50','50']"> <a href="#" class="btn btn-styl1">Read More</a></div>

                        <div class="tp-caption sl-address"
                        data-x="['left','left','left','center']" data-hoffset="['0','4','4','15']"
                        data-y="['middle','middle','middle','middle']" data-voffset="['325','280','250','200']"
                        data-width="full"
                        data-height="none"
                        data-whitespace="normal"
                        data-transform_idle="o:1;"
                        data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                        data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                        data-mask_in="x:0px;y:[100%];" 
                        data-mask_out="x:inherit;y:inherit;" 
                        data-start="1000" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on" 
                        data-paddingtop= "['50','50','50','50']"
                        data-paddingbottom= "['50','50','50','50']"> <a href="#" class="text-white sl-phone"><i class="fa fa-phone" aria-hidden="true"></i> +91 254 785 587</a><a href="#" class="text-white sl-email"><i class="fa fa-envelope" aria-hidden="true"></i> educate@info.com</a></div>
                    </li>
                </ul>
            </div>
        </div> 
    </section><!-- flat-slider -->
    <section class="partner-clients partner-clients-style1"> 
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-12">
                    <div class="slide-client owl-carousel" data-auto="false" data-item="4" data-nav="false" data-dots="false" data-gap="0" style="visibility: hidden;">
                        <ul>
                            <li><img src="images/home1/01.png" alt="images"></li>
                        </ul>
                        <ul>
                            <li><img src="images/home1/02.png" alt="images"></li>
                        </ul>
                        <ul>
                            <li><img src="images/home1/03.png" alt="images"></li>
                        </ul>
                        <ul>
                            <li><img src="images/home1/04.png" alt="images"></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="iconbox-style1">
                        <div class="apply-admission">
                            <div class="apply-admission-wrap type2 bd-type1">
                                <div class="apply-admission-inner">
                                    <h2 class="title text-center">
                                        <span>Apply for admission</span>
                                    </h2>
                                    <div class="caption text-center">
                                        Make it more simple!
                                    </div>
                                    <div class="apply-sent apply-sent-style1">
                                        <form action="#" class="form-sent">
                                            <input type="text" placeholder="Enter your email ....">
                                            <button class="sent-button bg-cl3f4c99">
                                                <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- partner-clients -->
    <section class="flat-introduce flat-introduce-style1 clearfix" style="padding-top: 0;">
        <div class="container">
            <div class="col-left">
                <div class="videobox">
                    <a class="fancybox" data-type="iframe" href="https://www.youtube.com/embed/2Ge1GGitzLw?autoplay=1">
                        <img src="images/home1/5.jpg" alt="images">
                    </a>
                    <div class="elip-top">
                        <img src="images/home1/42.png" alt="images">
                    </div>
                    <div class="elip-bottom">
                        <img src="images/home1/42.png" alt="images">
                    </div>
                </div>
            </div>
            <div class="col-right">
                <div class="content-introduce content-introduce-style1">
                    <div class="title-section">
                        <p class="sub-title lt-sp25">About our university</p>
                        <div class="flat-title larger heading-type1">Take a tour</div>
                    </div>
                    <div class="content-introduce-inner">
                        <p>
                            Education is the most powerful weapon which you can use to change the world.” Education is the key to elimi-nating gender inequality, to reducing poverty.
                        </p>
                        <p>
                            to creating a sustainable planet, to preventing needless deaths and illness, and to fostering peace.
                        </p>
                        <div class="content-list">
                            <ul>
                                <li>
                                    <span class="text">
                                        Education is extremely important because you overcome superstitions.
                                    </span>
                                </li>
                                <li>
                                    <span class="text">
                                        Education is only valuable if children are being taught right things.
                                    </span>
                                </li>
                                <li>
                                    <span class="text">  
                                        Education is not key to success in life.
                                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- flat-introduce -->
    <div class="title-section text-center">
        <!-- <p class="sub-title lt-sp17">Most popular courses</p> -->
        <div class="flat-title medium">
            Learning Method
        </div>
    </div>
    <section class="flat-services style1 parallax parallax1 clearfix">
        <div class="section-overlay"></div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4">
                    <div class="services-content-box themesflat-content-box" data-padding="0% 30% 0% 0%" data-mobipadding="0% 0% 0% 0%" data-smobipadding="0% 0% 0% 0%">
                        <div class="flat-imagebox imagebox-services style1">
                            <div class="imagebox-content">
                                <img src="images/home1/13.png" alt="images">
                                <h5 class="text-one text-white">Education Services</h5>
                                <p class="text-white">
                                    Learn and master development skills with real-world case studies and examples. 
                                </p>
                                <div class="read-more">
                                    <a href="#">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="services-content-box themesflat-content-box" data-padding="0% 15% 0% 16%" data-mobipadding="0% 0% 0% 0%" data-smobipadding="0% 0% 0% 0%">
                        <div class="flat-imagebox imagebox-services style1">
                            <div class="imagebox-content">
                                <img src="images/home1/14.png" alt="images">
                                <h5 class="text-two text-white">Success Rate</h5>
                                <p class="text-white">
                                    At Techpaathshala, our learning path ensures that our students gain 
                                </p>
                                <div class="read-more">
                                    <a href="#">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="services-content-box themesflat-content-box" data-padding="0% 0% 0% 30.5%" data-mobipadding="0% 0% 0% 0%" data-smobipadding="0% 0% 0% 0%">
                        <div class="flat-imagebox imagebox-services style1">
                            <div class="imagebox-content">
                                <img src="images/home1/15.png" alt="images">
                                <h5 class="text-three text-white">Foreign Student</h5>
                                <p class="text-white">
                                    expertise in the various tech components.
                                </p>
                                <div class="read-more">
                                    <a href="#">Read More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- flat-services -->
    <section class="flat-benefit style1 clearfix">
        <div class="container-fluid">
            <div class="col-12">
                <div class="wrap-inconbox-benefit">
                    <div class="title-section">
                        <div class="flat-title small heading-type2 text-white">Why choose us?</div>
                    </div>
                    <div class="iconbox-benefit iconbox-benefit-style1">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-sx-12">
                                <div class="themesflat-content-box" data-padding="0% 4% 0% 0%" data-sdesktoppadding="0% 0% 0% 0%" data-ssdesktoppadding="0% 0% 0% 0%"data-mobipadding="0% 0% 0% 0%" data-smobipadding="0% 0% 0% 0%">
                                    <div class="iconbox">
                                        <div class="iconbox-icon">
                                            <img src="images/home1/18.png" alt="images">
                                        </div>
                                        <div class="iconbox-content img-one">
                                            <h3>
                                                <a href="#">Interactive Session </a>
                                            </h3>
                                            <!-- <p>
                                                Education is the most power which you can to change the world.” Education is the key to.
                                            </p> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-sx-12">
                                <div class="themesflat-content-box" data-padding="0% 4% 0% 0%" data-sdesktoppadding="0% 15px 0% 0%" data-ssdesktoppadding="0% 15px 0% 0%"data-mobipadding="0% 0% 0% 0%" data-smobipadding="0% 0% 0% 0%">
                                    <div class="iconbox">
                                        <div class="iconbox-icon">
                                            <img src="images/home1/19.png" alt="images">
                                        </div>
                                        <div class="iconbox-content img-two">
                                            <h3>
                                                <a href="#">Industry Insights </a>
                                            </h3>
                                            <!-- <p>
                                                Education is the most power which you can to change the world.” Education is the key to.
                                            </p> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-sx-12">
                                <div class="themesflat-content-box" data-padding="0% 4% 0% 0%" data-sdesktoppadding="0% 0% 0% 0%" data-ssdesktoppadding="0% 0% 0% 0%"data-mobipadding="0% 0% 0% 0%" data-smobipadding="0% 0% 0% 0%">
                                    <div class="iconbox">
                                        <div class="iconbox-icon">
                                            <img src="images/home1/20.png" alt="images">
                                        </div>
                                        <div class="iconbox-content img-three">
                                            <h3>
                                                <a href="#">100% Placement Assistance</a>
                                            </h3>
                                            <!-- <p>
                                                Education is the most power which you can to change the world.” Education is the key to.
                                            </p> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-sx-12">
                                <div class="themesflat-content-box" data-padding="0% 4% 0% 0%" data-sdesktoppadding="0% 15px 0% 0%" data-ssdesktoppadding="0% 15px 0% 0%"data-mobipadding="0% 0% 0% 0%" data-smobipadding="0% 0% 0% 0%">
                                    <div class="iconbox">
                                        <div class="iconbox-icon">
                                            <img src="images/home1/21.png" alt="images">
                                        </div>
                                        <div class="iconbox-content img-four">
                                            <h3>
                                                <a href="#">Live Projects</a>
                                            </h3>
                                            <!-- <p>
                                                Education is the most power which you can to change the world.” Education is the key to.
                                            </p> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-sx-12">
                                <div class="themesflat-content-box" data-padding="0% 4% 0% 0%" data-sdesktoppadding="0% 15px 0% 0%" data-ssdesktoppadding="0% 15px 0% 0%"data-mobipadding="0% 0% 0% 0%" data-smobipadding="0% 0% 0% 0%">
                                    <div class="iconbox">
                                        <div class="iconbox-icon">
                                            <img src="images/home1/21.png" alt="images">
                                        </div>
                                        <div class="iconbox-content img-four">
                                            <h3>
                                                <a href="#">(Time frame)Program </a>
                                            </h3>
                                            <!-- <p>
                                                Education is the most power which you can to change the world.” Education is the key to.
                                            </p> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-sx-12">
                                <div class="themesflat-content-box" data-padding="0% 4% 0% 0%" data-sdesktoppadding="0% 15px 0% 0%" data-ssdesktoppadding="0% 15px 0% 0%"data-mobipadding="0% 0% 0% 0%" data-smobipadding="0% 0% 0% 0%">
                                    <div class="iconbox">
                                        <div class="iconbox-icon">
                                            <img src="images/home1/21.png" alt="images">
                                        </div>
                                        <div class="iconbox-content img-four">
                                            <h3>
                                                <a href="#">Portfolio Building </a>
                                            </h3>
                                            <!-- <p>
                                                Education is the most power which you can to change the world.” Education is the key to.
                                            </p> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-benefit-right" style="display: none;">
                <div class="apply-admission bg-apply-type1">
                    <div class="apply-admission-wrap type3 bd-type2">
                        <div class="apply-admission-inner">
                            <h2 class="title text-center">
                                <span>Apply for admission</span>
                            </h2>
                        </div>
                    </div>
                    <div class="form-apply">
                        <div class="section-overlay183251"></div>
                        <form action="#" class="apply-now">
                            <ul>
                                <li><input type="search" placeholder="Name"></li>
                                <li><input type="search" placeholder="Email"></li>
                                <li><input type="search" placeholder="Phone"></li>
                            </ul>
                            <div class="btn-50 hv-border text-center">
                                <button class="btn bg-clff5f60">
                                    Apply now
                                </button>
                            </div>
                        </form>  
                    </div>
                </div>
            </div>
        </div>
    </section><!-- flat-benefit -->
    <section class="flat-event flat-event-style1 clearfix" style="display: none;">
        <div class="container-fluid">
            <div class="col-left">
                <div class="content-event-style1 themesflat-content-box" data-padding="13.7% 1.2% 0% 0%" data-sdesktoppadding="0% 0% 0% 0%" data-ssdesktoppadding="0% 0% 0% 0%" data-mobipadding="0% 0% 0% 0%" data-smobipadding="0% 0% 0% 0%">
                    <div class="title-section">
                        <div class="flat-title larger heading-type3">All upcoming events</div>
                    </div>
                    <div class="content-event">
                        <div class="entry-info clearfix">
                            <div class="entry-title">
                                <a href="#" class="cl-7ecc88">
                                    International Conference on Business .....
                                </a>
                            </div>
                            <div class="entry-meta">
                                <ul>
                                    <li class="date clearfix">
                                        <span class="icon-event icon-icons8-planner-100"></span>
                                        <span class="detail-event">July 8, 2018</span>
                                    </li>
                                    <li class="time clearfix">
                                        <span class="icon-event icon-icons8-stopwatch-100"></span>
                                        <span class="detail-event">5.00pm - 7.00pm</span>
                                    </li>
                                    <li class="location clearfix">
                                        <span class="icon-event icon-icons8-marker-100"></span>
                                        <span class="detail-event">United States</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="entry-number number-one">
                            <span class="cl-7ecc88">1</span>
                        </div>
                    </div>
                    <div class="content-event">
                        <div class="entry-info clearfix">
                            <div class="entry-title">
                                <a href="#" class="cl-3f4c99">
                                    International Conference on Business .....
                                </a>
                            </div>
                            <div class="entry-meta">
                                <ul>
                                    <li class="date clearfix">
                                        <span class="icon-event icon-icons8-planner-100"></span>
                                        <span class="detail-event">July 8, 2018</span>
                                    </li>
                                    <li class="time clearfix">
                                        <span class="icon-event icon-icons8-stopwatch-100"></span>
                                        <span class="detail-event">5.00pm - 7.00pm</span>
                                    </li>
                                    <li class="location clearfix">
                                        <span class="icon-event icon-icons8-marker-100"></span>
                                        <span class="detail-event">United States</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="entry-number number-two">
                            <span class="cl-3f4c99">2</span>
                        </div>
                    </div>
                    <div class="content-event">
                        <div class="entry-info clearfix">
                            <div class="entry-title">
                                <a href="#" class="cl-ff5f60">
                                    International Conference on Business .....
                                </a> 
                            </div>
                            <div class="entry-meta">
                                <ul>
                                    <li class="date clearfix">
                                        <span class="icon-event icon-icons8-planner-100"></span>
                                        <span class="detail-event">July 8, 2018</span>
                                    </li>
                                    <li class="time clearfix">
                                        <span class="icon-event icon-icons8-stopwatch-100"></span>
                                        <span class="detail-event">5.00pm - 7.00pm</span>
                                    </li>
                                    <li class="location clearfix">
                                        <span class="icon-event icon-icons8-marker-100"></span>
                                        <span class="detail-event">United States</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="entry-number number-three">
                            <span class="cl-ff5f60">3</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-right">
                <div class="images-list themesflat-content-box" data-padding="0% 0% 0% 15.1%" data-sdesktoppadding="0% 0% 0% 5%" data-ssdesktoppadding="0% 0% 0% 0%" data-mobipadding="0% 0% 0% 0%" data-smobipadding="0% 0% 0% 0%">
                    <div class="images-list-1">
                        <div class="img-event">
                            <img src="images/home1/24.png" alt="images">
                            <span class="number bg-cl7ecc88">1</span>
                        </div>
                        <div class="img-event">
                            <img src="images/home1/25.png" alt="images">
                            <span class="number bg-cl3f4c99">2</span>
                        </div>
                    </div>
                    <div class="images-list-2">
                        <div class="img-event">
                            <img src="images/home1/26.png" alt="images">
                            <span class="number bg-clff5f60">3</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- flat-event -->
    <section class="testimonial testimonial-style2">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 max-width100">
                    <div class="wrap-testimonial">
                        <div class="title-section">
                            <div class="flat-title small heading-type5">
                               Something Special
                            </div>
                        </div>
                        <div class="client-style2 clearfix">
                            <div class="flat-carousel-box data-effect clearfix" data-gap="30" data-column="1" data-column2="1" data-column3="1" data-column4="1" data-dots="true" data-auto="false" data-nav="false">
                                <div class="owl-carousel">
                                    <div class="testimonial-content">
                                        <div class="text text-white">
                                        Amazing place to improve programming skills.It is the best place for 
                                        the freshers to learn coding and to start career with.Also as a fresher you 
                                        will get chance to learn different technologies under the perfect guidance of suhail sir
                                        </div>
                                        <div class="author-info">
                                            <h5>
                                                <a href="#">Munaf Hajir</a>
                                            </h5>
                                       
                                        </div>
                                    </div>
                                    <div class="testimonial-content">
                                        <div class="text text-white">
                                        I learnt more than I learned in college. The technically training sessions helped a lot.
                                        A mentor was was always available to solve my doubts whenever I was stuck somewhere. 
                                        I also got a high paying job through their placement drives. Totally recommend it
                                        </div>
                                        <div class="author-info">
                                            <h5>
                                                <a href="#">Abhay Vishwakarma</a>
                                            </h5>
                                        </div>
                                    </div>
                                    <div class="testimonial-content">
                                        <div class="text text-white">
                                        TechPaathshala helped me understand the complexity involved in preparing industry level projects with industry insights. 
                                        Learing and getting placed through TechPaathshala was truly a rewaring experience
                                        </div>
                                        <div class="author-info">
                                            <h5>
                                                <a href="#">Suleman Khan</a>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- testimonial -->
    <?php include 'footer/footer.php';?>
</body>

</html>