<?php include 'header/header_2.php' ;?>
    <div class="courses-single-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="content-page-wrap clearfix">
                        <div class="course-single">
                            <div class="featured-post">
                                <div class="entry-image">
                                    <div class="videobox">
                                        <a class="fancybox" data-type="iframe" href="https://www.youtube.com/embed/2Ge1GGitzLw?autoplay=1">
                                            <img src="images/course-single/1.jpg" alt="images">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="#">Learn Angular</a>
                                </div>
                            </div>
                        </div>
                        <div class="flat-tabs">
                            <ul class="tab-title type1 clearfix"> 
                                <li class="item-title  overview">
                                    <span class="inner">OVERVIEW</span>
                                </li>
                                <li class="item-title curriculum">
                                    <span class="inner">CURRICULUM</span>
                                </li>
                            </ul>
                            <div class="tab-content-wrap">
                                <div class="tab-content">
                                    <div class="item-content">
                                        <div class="question-sg text clearfix">
                                            <div class="title">
                                                <a href="#">Program Overview</a>
                                            </div>
                                            <p>
                                                The PHP Hypertext Preprocessor (PHP) is a programming language that allows web developers to create dynamic content that interacts with databases.
                                                This course It’s fantastic for someone who has the fundamentals down but wants to explore more advanced concepts and technologies to be successful as a developer.
                                                In this course, you'll learn the most popular front end library React and master the fundamentals around the state, props and the component lifecycle.

                                            </p>
                                        </div>
                                   
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div class="item-content">
                                        <div class="question-sg text clearfix">
                                            <div class="title">
                                                <a href="#">
                                                    Introduction to php</a>
                                            </div>
                                            <p>
                                               Php Fundamentals review & Functions, Date and Time, Debugging & Logging.
                                               This first module provides an overview of PHP, functions, debugging & logging in PHP.
                                            </p>
                                        </div>
                           
                                        <div class="certificate-sg text clearfix">
                                            <div class="title">
                                                <a href="#">You'll learn:</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Call by value vs Call by reference
                                                        </li>
                                                        <li>
                                                            Date & Time manipulation
                                                        </li>
                                                        <li>
                                                            Php built-in Constants, Debugging PHP and Application logging
                                                        </li></li>
                                                   
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Strings, Runtime Error Handling & File Resources</a>
                                            </div>
                                            <p>
                                                In this module, you’ll learn about the different types of strings, error handling, string manipulation functions.
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            String manipulation & Regular expressions
                                                        </li>
                                                        <li>
                                                            String manipulation functions
                                                        </li>
                                                        <li>
                                                            Regular expression functions
                                                        </li>
                                                        <li>
                                                            Runtime error handling
                                                        </li>
                                                        <li>
                                                            Manipulating files with PHP
                                                        </li>
                                                        <li>
                                                            Reading and parsing data in comma-separated value format
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Database Access & Mail Function </a>
                                            </div>
                                            <p>
                                                In this module,  you will learn how to access data and how to use PHP's built-in mail() function, 
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Database connectivity 
                                                        </li>
                                                        <li>
                                                            Mail function
                                                        </li>
                                                        <li>
                                                            Retrieving & sending E-mail using a PHP function
                                                        </li>
                                                        <li>
                                                            Php extension & Application Repository
                                                        </li>
                                                        <li>
                                                            Php extension & Application Repository

                                                        </li>
                                                        <li>
                                                            Client URL
                                                        </li>
                                                        <li>
                                                            Webscraping using cURL
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Web Services </a>
                                            </div>
                                            <p>
                                                This module is a walk through PHP and the implementation of SOAP.
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Simple  object access protocol
                                                        </li>
                                                        <li>
                                                            Web services description language 
                                                        </li>
                                                        <li>
                                                            Benefits of using WSDL
                                                        </li>
                                                        <li>
                                                            Developing web services using SOAP protocol
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Sessions</a>
                                            </div>
                                            <p>
                                                In this module, you will learn how to store certain data on the server using the PHP session.
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            PHP sessions
                                                        </li>
                                                        <li>
                                                            PHP sessions mechanism 
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">PHP Framework</a>
                                            </div>
                                            <p>
                                                In this module, you will understand uniqueness and similarity among different PHP frameworks
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            CakePHP
                                                        </li>
                                                        <li>
                                                            CodeIgniter
                                                        </li>
                                                        <li>
                                                            Yii
                                                        </li>
                                                        <li>
                                                            Zend
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div><!-- courses-single -->
    <div class="cta-cr parallax parallax3">
        <div class="overlay183251"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
                    <div class="cta-content">
                        <div class="caption">How to start your teaching?</div>
                        <h3>
                            Starting your journey with us? Follow this guide still possible to become a teacher.
                        </h3>
                        <div class="btn-about-become">
                            <a href="#">Become a Teacher</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
                    <div class="cta-information">
                        <div class="phone">
                            +91 254 785 587
                        </div>
                        <div class="email">
                            edukin@info.com
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- cta-cr -->
    <?php include 'footer/footer.php'; ?>
</body>

<!-- Mirrored from corpthemes.com/html/edukin/course-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Mar 2020 07:11:11 GMT -->
</html>