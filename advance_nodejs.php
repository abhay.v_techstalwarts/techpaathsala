<?php include 'header/header_2.php' ;?>
    <div class="courses-single-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="content-page-wrap clearfix">
                        <div class="course-single">
                            <div class="featured-post">
                                <div class="entry-image">
                                    <div class="videobox">
                                        <a class="fancybox" data-type="iframe" href="https://www.youtube.com/embed/2Ge1GGitzLw?autoplay=1">
                                            <img src="images/course-single/1.jpg" alt="images">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="#">Learn node js</a>
                                </div>
                            </div>
                        </div>
                        <div class="flat-tabs">
                            <ul class="tab-title type1 clearfix"> 
                                <li class="item-title  overview">
                                    <span class="inner">OVERVIEW</span>
                                </li>
                                <li class="item-title curriculum">
                                    <span class="inner">CURRICULUM</span>
                                </li>
                            </ul>
                            <div class="tab-content-wrap">
                                <div class="tab-content">
                                    <div class="item-content">
                                        <div class="question-sg text clearfix">
                                            <div class="title">
                                                <a href="#">Program Overview</a>
                                            </div>
                                            <p>
                                                Become a master at coding with our Full Stack Developer Course and get 100% placement assistance in some of the top companies.
                                                This course, specially designed for beginners by industry experts, aims to make you an expert in 
                                                10+ technologies that are used to make interactive websites, apps and amazing games. 
                                                You will get to learn the core technologies through practical training by mentors who have actually worked in the programming industry. 
                                            </p>
                                        </div>
                                   
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div class="item-content">
                                        <div class="question-sg text clearfix">
                                            <div class="title">
                                                <a href="#">
                                                    The Internals Of Node
                                                </a>
                                            </div>
                                            <p>
                                                In this module will learn all about the internal node & node's implementation.
                                            </p>
                                        </div>
                           
                                        <div class="certificate-sg text clearfix">
                                            <div class="title">
                                                <a href="#">You'll learn:</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            The basics of threads
                                                        </li>
                                                        <li>The node event loop implementation</li>
                                                        <li>Thread pools with multithreading</li>
                                                        <li>OS operations</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Enhancing Node Performance</a>
                                            </div>
                                            <p>
                                                This module will make you expert in creating webpages using Node.js
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Express setup
                                                        </li>
                                                        <li>Clustering in Action</li>
                                                        <li>Benchmarking server performance</li>
                                                        <li>Webworker threads</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Project Setup</a>
                                            </div>
                                            <p>
                                                In this module, you will walkthrough to project setup and creation
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Project walkthrough
                                                        </li>
                                                        <li>Key customization </li>
                                                        <li>MongoDB creation</li>
                                                        <li>Routes walkthrough </li>
                                                        <li></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Data caching with Redis </a>
                                            </div>
                                            <p>
                                                Learn all about Data caching and Redis configuration.
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            MongoDB query performance
                                                        </li>
                                                        <li>
                                                            Redis introduction
                                                        </li>
                                                        <li>
                                                            Cache keys, Functions, Issues, Solution & Implementation
                                                        </li>
                                                        <li>
                                                            Restoring Redis config
                                                        </li>
                                                        <li>
                                                            Hydrating Models & Arrays
                                                        </li>
                                                        <li>
                                                            Automated Cache Clearing with Middleware
                                                        </li>
                                                      
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Automated Headless Browser Testing </a>
                                            </div>
                                            <p>
                                                This module will give you an overall idea about Testing flow & how it works
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Testing Flow
                                                        </li>
                                                        <li>
                                                            Commands Around Testing
                                                        </li>
                                                        <li>
                                                            Launching Chromium Instances
                                                        </li>
                                                        <li>
                                                            DRY Tests
                                                        </li>

                                                        <li>
                                                            Chromium Navigation
                                                        </li>
                                                        <li>
                                                            Solving Authentication Issues with Automation Testing 
                                                        </li>
                                                        <li>
                                                            Proxies in Action
                                                        </li>
                                                      
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Writing Up Continuous Integration  </a>
                                            </div>
                                            <p>
                                                In this module, you will learn about CI builds.
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Introduction to CI
                                                        </li>
                                                        <li>
                                                            CI Providers
                                                        </li>
                                                        <li>
                                                            The Basics of YAML Files &  Setup
                                                        </li>
                                                        <li>
                                                            Client Builds
                                                        </li>
                                                        <li>
                                                            Script Config
                                                        </li>
                                                        <li>
                                                            Tarvis CI Setup
                                                        </li>
                                                        <li>
                                                            Triggering CI Builds
                                                        </li>
                                                      
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Scalable Image/File Upload  </a>
                                            </div>
                                            <p>
                                                Learn to upload files and configuration of AWS SDK
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Image Upload 
                                                        </li>
                                                        <li>
                                                            Chosen Storage Solution
                                                        </li>
                                                        <li>
                                                            Image File Transport
                                                        </li>
                                                        <li>
                                                            Handling File Changes
                                                        </li>
                                                        <li>
                                                            Configuring the AWS SDK
                                                        </li>
                                                        <li>
                                                            Upload Routes Files
                                                        </li>
                                                        <li>
                                                            S3 Bucket Policies
                                                        </li>
                                                      
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>  
                                
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div><!-- courses-single -->
    <div class="cta-cr parallax parallax3">
        <div class="overlay183251"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
                    <div class="cta-content">
                        <div class="caption">How to start your teaching?</div>
                        <h3>
                            Starting your journey with us? Follow this guide still possible to become a teacher.
                        </h3>
                        <div class="btn-about-become">
                            <a href="#">Become a Teacher</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
                    <div class="cta-information">
                        <div class="phone">
                            +91 254 785 587
                        </div>
                        <div class="email">
                            edukin@info.com
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- cta-cr -->
    <?php include 'footer/footer.php'; ?>
</body>

<!-- Mirrored from corpthemes.com/html/edukin/course-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Mar 2020 07:11:11 GMT -->
</html>