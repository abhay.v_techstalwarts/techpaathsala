<?php include 'header/header_2.php' ;?>
    <div class="courses-single-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="content-page-wrap clearfix">
                        <div class="course-single">
                            <div class="featured-post">
                                <div class="entry-image">
                                    <div class="videobox">
                                        <a class="fancybox" data-type="iframe" href="https://www.youtube.com/embed/2Ge1GGitzLw?autoplay=1">
                                            <img src="images/course-single/1.jpg" alt="images">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="#">Learn web designer</a>
                                </div>
                            </div>
                        </div>
                        <div class="flat-tabs">
                            <ul class="tab-title type1 clearfix"> 
                                <li class="item-title  overview">
                                    <span class="inner">OVERVIEW</span>
                                </li>
                                <li class="item-title curriculum">
                                    <span class="inner">CURRICULUM</span>
                                </li>
                            </ul>
                            <div class="tab-content-wrap">
                                <div class="tab-content">
                                    <div class="item-content">
                                        <div class="question-sg text clearfix">
                                            <div class="title">
                                                <a href="#">Program Overview</a>
                                            </div>
                                            <p>
                                                Learn to Code Advanced HTML & CSS takes a deeper look at front-end design and development. Studying modern front-end development, this guide teaches the latest for any designer looking to round out their front-end skills.

                                            </p>
                                        </div>
                                   
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div class="item-content">
                                        <div class="question-sg text clearfix">
                                            <div class="title">
                                                <a href="#">
                                                    HTML 5:</a>
                                            </div>
                                            <!-- <p>
                                                Review of core AngularJS key concepts
                                            </p> -->
                                        </div>
                           
                                        <div class="certificate-sg text clearfix">
                                            <div class="title">
                                                <a href="#">You'll learn:</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            migration from html4 to html5
                                                        </li>
                                                        <li>
                                                            font download generate
                                                        </li>
                                                        <li>
                                                            HTML 5 tags in details with example
                                                        </li>
                                                        <li>
                                                            navigation bar
                                                        </li>
                                                        <li>
                                                            image gallery
                                                        </li>
                                                   
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">CSS</a>
                                            </div>
                                            <!-- <p>
                                                In this module, you will learn to build and style medium clap
                                            </p><br> -->
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            box-shadow
                                                        </li>
                                                        <li>
                                                            text-shadow
                                                        </li>
                                                        <li>
                                                            gray scale
                                                        </li>
                                                        <li>
                                                            gradient
                                                        </li>
                                                        <li>
                                                            border
                                                        </li>
                                                        <li>
                                                            rounder corner
                                                        </li>
                                                        <li>
                                                            border image
                                                        </li>
                                                        <li>
                                                            2D animation effect
                                                        </li>
                                                        <li>
                                                            3D animation effect
                                                        </li>
                                                        <li>
                                                            transition
                                                        </li>
                                                        <li>
                                                            key-frame effect
                                                        </li>
                                                        <li>
                                                            Opacity
                                                        </li>
                                                        <li>
                                                            sprite image
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Jquery</a>
                                            </div>
                                            <!-- <p>
                                                Building your own Hooks lets you extract component logic into reusable functions. This module provides an overview of Hooks for experienced React users.
                                            </p><br> -->
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            document ready
                                                        </li>
                                                        <li>
                                                            slider- bxslider, owl-slider, slick-slider
                                                        </li>
                                                        <li>
                                                            js add and remove class
                                                        </li>
                                                        <li>
                                                            toggle class
                                                        </li>
                                                        <li>
                                                            light box
                                                        </li>
                                                        <li>
                                                            js scroll to top
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Forms</a>
                                            </div>
                                            <!-- <p>
                                                This module aims to help you understand the compound components pattern so that you can use it effectively with confidence and clarity.
                                            </p><br> -->
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Custom form validation
                                                        </li>
                                                        <li>
                                                            Custom form control
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Services</a>
                                            </div>
                                            <!-- <p>
                                                This module is all about how to implement the styling of React components.
                                            </p><br> -->
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Provider Service
                                                        </li>
                                                        <li>
                                                            Service Decorator
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Web Services</a>
                                            </div>
                                            <!-- <p>
                                                In this module, you will learn what a container component is and also how it works.
                                            </p><br> -->
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Restangular
                                                        </li>
                                                        <li>
                                                            Messaging
                                                        </li>
                                                        <li>
                                                            Promises
                                                        </li>
                                                      
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Best Practices with Angular</a>
                                            </div>
                                            <!-- <p>
                                                The data inside React Components are managed by state and props. After understanding props, in this module, you will learn about the state.
                                            </p><br> -->
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Angular JS design Patterns
                                                        </li>
                                                        <li>
                                                            Effective angular testing strategies
                                                        </li>
                                                        <li>
                                                            Controller-as-syntax


                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div><!-- courses-single -->
    <div class="cta-cr parallax parallax3">
        <div class="overlay183251"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
                    <div class="cta-content">
                        <div class="caption">How to start your teaching?</div>
                        <h3>
                            Starting your journey with us? Follow this guide still possible to become a teacher.
                        </h3>
                        <div class="btn-about-become">
                            <a href="#">Become a Teacher</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
                    <div class="cta-information">
                        <div class="phone">
                            +91 254 785 587
                        </div>
                        <div class="email">
                            edukin@info.com
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- cta-cr -->
    <?php include 'footer/footer.php'; ?>
</body>

<!-- Mirrored from corpthemes.com/html/edukin/course-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Mar 2020 07:11:11 GMT -->
</html>