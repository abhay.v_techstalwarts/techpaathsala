<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from corpthemes.com/html/edukin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Mar 2020 07:02:54 GMT -->
<head>
    <meta charset="UTF-8">
    <title>Techpathshala</title>

    <!-- Mobile Specific Metas-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <!-- Bootstrap-->
    <link rel="stylesheet" href="stylesheet/bootstrap.css">

    <!-- Template Style-->
    <link rel="stylesheet" href="stylesheet/font-awesome.css"> 
    <link rel="stylesheet" href="stylesheet/animate.css">
    <link rel="stylesheet" href="stylesheet/style.css">
    <link rel="stylesheet" href="stylesheet/shortcodes.css">
    <link rel="stylesheet" href="stylesheet/jquery-fancybox.css">
    <link rel="stylesheet" href="stylesheet/responsive.css">
    <link rel="stylesheet" href="stylesheet/flexslider.css">
    <link rel="stylesheet" href="stylesheet/owl.theme.default.min.css">
    <link rel="stylesheet" href="stylesheet/owl.carousel.min.css">
    <link rel="stylesheet" href="stylesheet/jquery.mCustomScrollbar.min.css">
    
    
    <link href="icon/favicon.ico" rel="shortcut icon">
</head>
<body>
    <div id="loading-overlay">
        <div class="loader"></div>
    </div>
    <div class="wrap-header">
        <header class="header flat-header lh-header header-style1">
            <div class="site-header-inner">
                <div class="container">
                    <div id="logo" class="logo">
                        <a href="index.php"><img src="images/logo/02.png" alt="images"></a>
                    </div>
                    <div class="mobile-button"><span></span></div>
                    <div class="header-menu">
                        <nav id="main-nav" class="main-nav">
                            <ul class="menu">
                                <li><a href="index.php">home</a>
                                </li>
                                <li><a href="#">Program</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item"><a href="foundation.php">Foundation</a></li>
                                        <li class="menu-item"><a href="advance.php">Advance</a></li>
                                    </ul><!-- sub-menu -->
                                </li>
                                <li><a href="#">Placement</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item"><a href="job_intern.php">Job / Internship</a></li>
                                        <li class="menu-item"><a href="recruit.php">Recuruit From Us</a></li>
                                    </ul><!-- sub-menu -->
                                </li>
                                <li><a href="contact.php">Contact</a>
                                </li>
                            </ul>
                        </nav>
                    </div> 
                </div>
            </div>
        </header><!-- header -->
    </div><!-- wrap-header -->

    </html>