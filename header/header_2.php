<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from corpthemes.com/html/edukin/course.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Mar 2020 07:10:24 GMT -->
<head>
    <meta charset="UTF-8">
    <title>techpathshala</title>

    <!-- Mobile Specific Metas-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <!-- Bootstrap-->
    <link rel="stylesheet" href="stylesheet/bootstrap.css">

    <!-- Template Style-->
    <link rel="stylesheet" href="stylesheet/font-awesome.css"> 
    <link rel="stylesheet" href="stylesheet/animate.css">
    <link rel="stylesheet" href="stylesheet/style.css">
    <link rel="stylesheet" href="stylesheet/shortcodes.css">
    <link rel="stylesheet" href="stylesheet/jquery-fancybox.css">
    <link rel="stylesheet" href="stylesheet/responsive.css">
    <link rel="stylesheet" href="stylesheet/flexslider.css">
    <link rel="stylesheet" href="stylesheet/owl.theme.default.min.css">
    <link rel="stylesheet" href="stylesheet/owl.carousel.min.css">
    <link rel="stylesheet" href="stylesheet/jquery.mCustomScrollbar.min.css">

    <link href="icon/favicon.ico" rel="shortcut icon">
</head>
<body>
    <div id="loading-overlay">
        <div class="loader"></div>
    </div>
    <div class="bg-header">
        <div class="flat-header-blog">
            <div class="top-bar clearfix">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-8 col-sm-12 col-xs-12">
                            <ul class="information">
                                <li class="phone lt-sp003">
                                    <i class="fa fa-phone" aria-hidden="true"></i> +91 99306 02530
                                </li>
                                <li class="email">
                                    <i class="fa fa-envelope" aria-hidden="true"></i> Business@techstalwarts.com
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-6 col-md-4 col-sm-12 col-xs-12">
                            <ul class="nav-sing">
                                <!-- <li><a href="#">Sing In</a></li>
                                <li><a href="#">Sing Up</a></li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <header class="header menu-bar header-blog hv-menu-type2">
                <div class="container"> 
                    <div class="menu-bar-wrap clearfix">
                        <div id="logo" class="logo">
                            <a href="index.html"><img src="images/logo/02.png" alt="images"></a>
                        </div>
                        <div class="mobile-button"><span></span></div>
                        <div class="header-menu">
                            <nav id="main-nav" class="main-nav">
                                <ul class="menu">
                                    <li><a href="index.php">Home</a>
                                    </li>
                                    <li><a href="#">Program</a>
                                        <ul class="sub-menu">
                                            <li class="menu-item"><a href="foundation.php">Foundation</a></li>
                                            <li class="menu-item"><a href="advance.php">Advance</a></li>
                                        </ul><!-- sub-menu -->
                                    </li>
                                    <li><a href="#">Placement</a>
                                        <ul class="sub-menu">
                                            <li class="menu-item"><a href="job_intern.php">Job / Internship</a></li>
                                            <li class="menu-item"><a href="recruit.php">Recuruit From Us</a></li>
                                        </ul><!-- sub-menu -->
                                    </li>
                                    <li><a href="contact.php">Contact</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
            <div class="page-title page-title-blog">
                <div class="page-title-inner">
                    <div class="breadcrumbs breadcrumbs-blog text-left">
                        <div class="container">
                            <div class="breadcrumbs-wrap">
                                <ul class="breadcrumbs-inner">
                                    <li><a href="index.php">Home</a></li>
                                    <li><a href="course.php" id="common_heading"></a></li>
                                </ul>
                                <div class="title" id="common_heading2">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- bg-header -->