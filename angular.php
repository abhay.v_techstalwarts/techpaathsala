<?php include 'header/header_2.php' ?>
    <div class="courses-single-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="content-page-wrap clearfix">
                        <div class="course-single">
                            <div class="featured-post">
                                <div class="entry-image">
                                    <div class="videobox">
                                        <a class="fancybox" data-type="iframe" href="https://www.youtube.com/embed/2Ge1GGitzLw?autoplay=1">
                                            <img src="images/course-single/1.jpg" alt="images">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="content">
                                <div class="title">
                                    <a href="#">Learn Angular</a>
                                </div>
                            </div>
                        </div>
                        <div class="flat-tabs">
                            <ul class="tab-title type1 clearfix"> 
                                <li class="item-title  overview">
                                    <span class="inner">OVERVIEW</span>
                                </li>
                                <li class="item-title curriculum">
                                    <span class="inner">CURRICULUM</span>
                                </li>
                            </ul>
                            <div class="tab-content-wrap">
                                <div class="tab-content">
                                    <div class="item-content">
                                        <div class="question-sg text clearfix">
                                            <div class="title">
                                                <a href="#">Program Overview</a>
                                            </div>
                                            <p>
                                                Angular 4 is a JavaScript framework for building web applications and apps in JavaScript, html, and TypeScript, which is a superset of JavaScript.
                                                This course, specially designed for beginners
                                                You will be introduced to Angular core concepts by industry experts which will help you to build powerful, dynamic web applications with ease.
                                            </p>
                                        </div>
                                   
                                    </div>
                                </div>
                                <div class="tab-content">
                                    <div class="item-content">
                                        <div class="question-sg text clearfix">
                                            <div class="title">
                                                <a href="#">Introduction to fundamentals</a>
                                            </div>
                                            <p>
                                                The course starts with understanding what makes Google’s AngularJS,an all-inclusive JavaScript model-view-controller (MVC) framework very easy to quickly build applications that run well on any desktop or mobile platform
                                            </p>
                                        </div>
                           
                                        <div class="certificate-sg text clearfix">
                                            <div class="title">
                                                <a href="#">You'll learn:</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            Object Oriented Design Fundamentals
                                                        </li>
                                                        <li>
                                                            Introduction to Javascript
                                                        </li>
                                                        <li>
                                                            Introduction to Angular JS 5.0
                                                        </li>
                                                        <li>
                                                            Learn about Grid System and Responsive Web Design
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Angular development components</a>
                                            </div>
                                            <p>
                                                This module will give you idea of what MVC means and how The model, view, and controller are all clearly defined in AngularJS and serve to greatly simplify the development process.
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            AngularJS components
                                                        </li>
                                                        <li>
                                                            AngularJS directives
                                                        </li>
                                                        <li>
                                                            AngularJS services
                                                        </li>
                                                        <li>
                                                            AngularJS resources
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Ionic Framework</a>
                                            </div>
                                            <p>
                                                Module 3 is all about setting up our environment, install Ionic and start our first project. We prepare our environment to build our app for iOS and Android mobile devices!
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            IONIC Setup
                                                        </li>
                                                        <li>
                                                            Ionic Architecture
                                                        </li>
                                                        <li>
                                                            Ionic Components
                                                        </li>
                                                        <li>
                                                            Cordova
                                                        </li>
                                                        <li>Cordova Plugins</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="description-single text clearfix">
                                            <div class="title">
                                                <a href="#">Building & Deployment of Hybrid App</a>
                                            </div>
                                            <p>
                                                Module 4 is all about advanced angular concepts,  Building hybrid app and procedure of deployment.
                                            </p><br>
                                            <div class="title">
                                                <a href="#">You'll learn</a>
                                            </div>
                                            <div class="certificate">
                                                <div class="certificate-wrap">
                                                    <ul class="list-certificate">
                                                        <li>
                                                            CSS Components
                                                        </li>
                                                        <li>
                                                            JS Components
                                                        </li>
                                                        <li>
                                                            Advanced Angular Concepts
                                                        </li>
                                                        <li>
                                                            Single View and Multi Tab Apps
                                                        </li>
                                                        <li>App deployment to Windows/iOS/Android</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- <div class="accordion-container">
                                            <h2>jQuery Accordion</h2>
                                            <div class="set">
                                              <a>
                                                You'll learn 
                                                <i class="fa fa-plus"></i>
                                              </a>
                                              <div class="content">
                                                <div class="certificate-sg text clearfix">
                                                    <div class="title">
                                                        <a href="#">You'll learn:</a>
                                                    </div>
                                                    <div class="certificate">
                                                        <div class="certificate-wrap">
                                                            <ul class="list-certificate">
                                                                <li>
                                                                    Object Oriented Design Fundamentals
                                                                </li>
                                                                <li>
                                                                    Introduction to Javascript
                                                                </li>
                                                                <li>
                                                                    Introduction to Angular JS 5.0
                                                                </li>
                                                                <li>
                                                                    Learn about Grid System and Responsive Web Design
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>
                                            <div class="set">
                                              <a>
                                                Ionic Framework: 
                                                <i class="fa fa-plus"></i>
                                              </a>
                                              <div class="content">
                                                <p> IONIC Setup</p>
                                                <p>Ionic Architecture</p>
                                                <p>Ionic Components</p>
                                                <p>Cordova</p>
                                                <p>Cordova Plugins</p>
                                              </div>
                                            </div>
                                            <div class="set">
                                              <a>
                                                Building & Deployment of Hybrid App 
                                                <i class="fa fa-plus"></i> 
                                              </a>
                                              <div class="content">
                                                <p> CSS Components </p>
                                                <p>JS Components</p>
                                                <p>Advanced Angular Concepts</p>
                                                <p>Single View and Multi Tab Apps</p>
                                                <p>App deployment to Windows/iOS/Android</p>
                                              </div>
                                            </div>
                                          </div> -->
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div><!-- courses-single -->
    <div class="cta-cr parallax parallax3">
        <div class="overlay183251"></div>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-7 col-sm-12 col-xs-12">
                    <div class="cta-content">
                        <div class="caption">How to start your teaching?</div>
                        <h3>
                            Starting your journey with us? Follow this guide still possible to become a teacher.
                        </h3>
                        <div class="btn-about-become">
                            <a href="#">Become a Teacher</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-12 col-xs-12">
                    <div class="cta-information">
                        <div class="phone">
                            +91 254 785 587
                        </div>
                        <div class="email">
                            edukin@info.com
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- cta-cr -->
    <?php include 'footer/footer.php' ?>
</body>

<!-- Mirrored from corpthemes.com/html/edukin/course-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Mar 2020 07:11:11 GMT -->
</html>