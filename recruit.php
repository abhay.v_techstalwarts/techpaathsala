<?php include 'header/header_1.php'; ?>
    
    <section class="flat-slider style1 clearfix">
        <div class="rev_slider_wrapper fullwidthbanner-container" >
            <div id="rev-slider1" class="rev_slider fullwidthabanner">
                <ul>
                    <!-- Slide 1 -->
                    <li data-transition="random">
                        <!-- Main Image -->
                        <img src="images/header/02.png" alt="" data-bgposition="center center" data-no-retina>
                        <div class="overlay"></div>

                        <!-- Layers -->
                        <div class="tp-caption tp-resizeme education"
                        data-x="['left','left','left','center']" data-hoffset="['0','4','4','15']"
                        data-y="['middle','middle','middle','middle']" data-voffset="['-113','-80','-70','-70']"
                        data-fontsize="['70','70','50','30']"
                        data-lineheight="['90','90','70','50']"
                        data-width="full"
                        data-height="none"
                        data-whitespace="normal"
                        data-transform_idle="o:1;"
                        data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                        data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                        data-mask_in="x:0px;y:[100%];" 
                        data-mask_out="x:inherit;y:inherit;" 
                        data-start="1000" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on"> <div class="education-text text-white text-center">Recruit From Us</div> </div>

                        <div class="tp-caption tp-resizeme text-white complete text-edukin text-center"
                        data-x="['left','left','left','center']" data-hoffset="['0','4','4','15']"
                        data-y="['middle','middle','middle','middle']" data-voffset="['-22','-5','-10','-10']"
                        data-fontsize="['17','17','15','14']"
                        data-lineheight="['30','30','26','22']"
                        data-width="full"
                        data-height="none"
                        data-whitespace="normal"
                        data-transform_idle="o:1;"
                        data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                        data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                        data-mask_in="x:0px;y:[100%];" 
                        data-mask_out="x:inherit;y:inherit;" 
                        data-start="1000" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on" 
                        data-paddingleft="['300','155','50','2']"
                        data-paddingright="['300','155','50','2']" >A talent pool of professional candidates awaits you. Choose the best from the best.</div>

                        <div class="tp-caption text-center"
                        data-x="['left','left','left','center']" data-hoffset="['0','4','4','15']"
                        data-y="['middle','middle','middle','middle']" data-voffset="['88','80','60','70']"
                        data-width="full"
                        data-height="none"
                        data-whitespace="normal"
                        data-transform_idle="o:1;"
                        data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                        data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                        data-mask_in="x:0px;y:[100%];" 
                        data-mask_out="x:inherit;y:inherit;" 
                        data-start="1000" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on" 
                        data-paddingtop= "['50','50','50','50']"
                        data-paddingbottom= "['50','50','50','50']"> <a href="#" class="btn btn-styl1 ">Start Hiring</a></div>

                        <!-- <div class="tp-caption sl-address"
                        data-x="['left','left','left','center']" data-hoffset="['0','4','4','15']"
                        data-y="['middle','middle','middle','middle']" data-voffset="['325','280','250','200']"
                        data-width="full"
                        data-height="none"
                        data-whitespace="normal"
                        data-transform_idle="o:1;"
                        data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" 
                        data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" 
                        data-mask_in="x:0px;y:[100%];" 
                        data-mask_out="x:inherit;y:inherit;" 
                        data-start="1000" 
                        data-splitin="none" 
                        data-splitout="none" 
                        data-responsive_offset="on" 
                        data-paddingtop= "['50','50','50','50']"
                        data-paddingbottom= "['50','50','50','50']"> <a href="#" class="text-white sl-phone"><i class="fa fa-phone" aria-hidden="true"></i> +91 254 785 587</a><a href="#" class="text-white sl-email"><i class="fa fa-envelope" aria-hidden="true"></i> educate@info.com</a></div> -->
                    </li>
                </ul>
            </div>
        </div> 
    </section><!-- flat-slider -->
    <section class="flat-benefit flat-benefit-style5 pt0 clearfix">
        <div class="container-fluid">
            <div class="col-benefit-right" style="min-width: 68% ; margin:0 auto !important ; float: none">
                <div class="apply-admission bg-apply-type1">
                    <div class="apply-admission-wrap type5 bd-type2">
                        <div class="apply-admission-inner">
                            <h2 class="title text-center">
                                <span>Our students hired by best tech companies</span>
                            </h2>
                        </div>
                    </div>
                    <div class="form-apply">
                        <div class="section-overlay333"></div>
                        <form action="recruit_save_data.php" class="apply-now" method="POST">
                            <div class="row">
                                <div class="col-md-6">
                                        <ul>
                                                <li><input type="search" name="name" placeholder="Name Of Company Representative" required></li>
                                                <li><input type="search" name="email" placeholder="Email" required></li>
                                                <li><input type="search" name="phone" placeholder="Phone" required></li>
                                                <li><input type="search" name="orgtype" placeholder="Organisation Type eg Client or Agency" required></li>

                                                
                                            </ul>
                                </div>
                                <div class="col-md-6">
                                        <ul>
                                            <li><input type="search" name="organisation" placeholder="Organisation Name" required></li>
                                            <li><input type="search" name="website" placeholder="Oranganisation Website" required></li>
                                            <li><input type="search" name="Streamprogram" placeholder="Looking For Hire Students From:( Stream of program)" required></li>
                                                <li><input type="search" name="nostudent" placeholder="Number of students want to hire" required></li>                                                
                                            </ul>
                                </div>
                            </div>
                            
                            <div class="btn-50 hv-border text-center">
                                <button class="btn bg-clf0c41b">
                                    Send
                                </button>
                            </div>
                        </form>  
                    </div>
                </div>
            </div>
        </div>
    </section>
  <?php include 'footer/footer.php'; ?>  
</body>

<!-- Mirrored from corpthemes.com/html/edukin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 Mar 2020 07:02:54 GMT -->
</html>